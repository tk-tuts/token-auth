/**
 * Created by Keerthikan on 26-Feb-17.
 */
var express = require('express');
let router = express.Router();

const auth = require('../config/jwt');

router.route('/')
    .get(auth, function (req, res, next) {
        res.send({
            name: "keerthikan",
            hobbies: ["cricket", "programming"]
        });
    });

module.exports = router;