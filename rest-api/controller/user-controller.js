/**
 * Created by Keerthikan on 26-Feb-17.
 */

const jwt = require('jsonwebtoken'),
    config = require( '../config/development');

const users = {
    user1: {
        _id : 1,
        password: "pass1"
    },
    user2: {
        _id : 2,
        password: "pass2"
    }
};

module.exports.authenticate = function(req, res, next){
    if(users.hasOwnProperty(req.body.username) && users[req.body.username].password == req.body.password){
        req.user = {
            _id : users[req.body.username]._id,
            username : req.body.username
        };
        next();
    }else{
        next(new Error("invalid username and password"));
    }
};

module.exports.generateToken = function generateToken(req, res, next) {
    if (!req.user) return next();

    const jwtPayload = {
        id: req.user._id,
        username : req.user.username
    };
    const jwtData = {
        algorithm: 'HS256',
        expiresIn: config.jwtDuration,
    };
    const secret = config.jwtSecret;
    req.token = jwt.sign(jwtPayload, secret, jwtData);

    next();
};

module.exports.respondJWT = function(req, res){
    if (!req.user) {
        res.status(401).json({
            error: 'Unauthorized'
        });
    } else {
        res.status(200).json({
            jwt: req.token
        });
    }
};