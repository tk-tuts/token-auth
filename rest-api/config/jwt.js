/**
 * Created by Keerthikan on 26-Feb-17.
 */
module.exports = require('express-jwt')({
   secret: require('./development').jwtSecret
});