/**
 * Created by Keerthikan on 26-Feb-17.
 */
module.exports = {
    env: 'development',
    jwtSecret: 'my-api-secret',
    jwtDuration: '15 minutes'
};